<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-15
  Time: 13:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <c:if test="${not empty mercData}">
        <title>Update</title>
    </c:if>
    <c:if test="${empty mercData}">
        <title>Create</title>
    </c:if>

</head>
<body>
<c:if test="${not empty mercData}">
    <h2>Update Data</h2>
    <c:url var="url" value="/updateMerchants"/>
    <form:form method="post" action="${url}">
        ID:<br>
        <input type="text" name="id" value="${mercData.id}">
        <br>
        Name:<br>
        <input type="text" name="name" value="${mercData.name}">
        <br>
        <br>
        <input type="submit" value="Submit">
    </form:form>
</c:if>
<c:if test="${empty mercData}">
    <h2>Create Data</h2>
    <div>${msg}</div>
    <c:url var="url" value="/createMerchants"/>
    <form:form method="post" action="${url}">
        ID:<br>
        <input type="text" name="id_">
        <br>
        Name:<br>
        <input type="text" name="name_">
        <br>
        <br>
        <input type="submit" value="Submit">
    </form:form>
</c:if>
</body>
</html>

