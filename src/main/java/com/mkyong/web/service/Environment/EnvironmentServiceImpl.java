package com.mkyong.web.service.Environment; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dao.Environment.EnvironmentDao;
import com.mkyong.web.dto.EnvironmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: EnvironmentServiceImpl.java, v 0.1 2019-03-13 21:45 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */

@Service
public class EnvironmentServiceImpl implements EnvironmentService {

    @Autowired
    EnvironmentDao environmentDao;

    @Override
    public EnvironmentDto findByEnvironmentId(String id, SqlMapClient sqlMapClient) {
        return environmentDao.getEnvironmentById(id, sqlMapClient);
    }

    @Override
    public List<EnvironmentDto> getAllEnvironment(SqlMapClient sqlMapClient) {
        return environmentDao.getEnvironment(sqlMapClient);
    }

    @Override
    public void deleteByEnvironmentId(String id, SqlMapClient sqlMapClient) {
        environmentDao.deleteEnvironment(id, sqlMapClient);
    }

    @Override
    public void createEnvironment(EnvironmentDto environmentDto, SqlMapClient sqlMapClient) {
        environmentDao.createEnvironment(environmentDto, sqlMapClient);
    }

    @Override
    public void updateByEnvironmentId(EnvironmentDto environmentDto, SqlMapClient sqlMapClient) {
        environmentDao.updateEnvironment(environmentDto, sqlMapClient);
    }
}