package com.mkyong.web.service.Login; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dao.LoginDao.LoginDao;
import com.mkyong.web.dto.LoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: LoginServiceImpl.java, v 0.1 2019-03-15 15:34 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Service
public class LoginServiceImpl implements LoginService{

    @Autowired
    LoginDao loginDao;

    @Override
    public LoginDto findUsernameAndPassword(LoginDto loginDto, SqlMapClient sqlMapClient) {
        return loginDao.validateLogin(loginDto, sqlMapClient);
    }
}