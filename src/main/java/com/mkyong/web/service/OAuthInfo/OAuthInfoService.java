package com.mkyong.web.service.OAuthInfo; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.OAuthInfoDto;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: OAuthInfoService.java, v 0.1 2019-03-14 15:40 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface OAuthInfoService {
    OAuthInfoDto findByOAuthInfoId(String id, SqlMapClient sqlMapClient);

    List<OAuthInfoDto> getAllOAuthInfo(SqlMapClient sqlMapClient);

    void deleteByOAuthInfoId(String id, SqlMapClient sqlMapClient);

    void createOAuthInfo(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient);

    void updateByOAuthInfoId(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient);
}