package com.mkyong.web.dao.LoginDao; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.LoginDto;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: LoginDao.java, v 0.1 2019-03-15 15:33 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface LoginDao {
    LoginDto validateLogin(LoginDto loginDto, SqlMapClient sqlMapClient);
}