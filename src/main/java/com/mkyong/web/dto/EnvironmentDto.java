package com.mkyong.web.dto; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: EnvironmentDto.java, v 0.1 2019-03-13 17:16 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public class EnvironmentDto {
    private String id;
    private String name;

    public EnvironmentDto() {
    }

    public EnvironmentDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Getter method for property id.
     *
     * @return property value of id
     **/
    public String getId() {
        return id;
    }

    /**
     * Setter method for property id.
     *
     * @param id value to be assigned to property id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter method for property name.
     *
     * @return property value of name
     **/
    public String getName() {
        return name;
    }

    /**
     * Setter method for property name.
     *
     * @param name value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }
}