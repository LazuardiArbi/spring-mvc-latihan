package com.mkyong.web.controller; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mkyong.web.dto.EnvironmentDto;
import com.mkyong.web.dto.MerchantDto;
import com.mkyong.web.dto.OAuthInfoDto;
import com.mkyong.web.service.Environment.EnvironmentService;
import com.mkyong.web.service.Merchent.MerchantService;
import com.mkyong.web.service.OAuthInfo.OAuthInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: OAuthInfoController.java, v 0.1 2019-03-15 13:49 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Controller
public class OAuthInfoController {


    /**
     * # = Berhasil jalan
     * O = Belum dicoba
     */

    @Autowired
    OAuthInfoService oAuthInfoService;

    @Autowired
    EnvironmentService environmentService;

    @Autowired
    MerchantService merchantService;

    /**
     * Get All Merchant Data #
     */
    @RequestMapping(value = "/authinfos", method = RequestMethod.GET)
    public String ShowAllMerchant(Model model) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        List<OAuthInfoDto> oAuthInfoDtoList = oAuthInfoService.getAllOAuthInfo(sqlmapClient);
        System.out.println(oAuthInfoDtoList);
        if (oAuthInfoDtoList.isEmpty()) {
            model.addAttribute("msg", "Data is Empty");
        }
        model.addAttribute("auth", oAuthInfoDtoList);

        return "oAuthInfosView";
    }

    /**
     * Get Merchant Data by Id #
     */
    @RequestMapping(value = "/authinfos/{id}", method = RequestMethod.GET)
    public String ShowMerchantById(Model model, @PathVariable String id) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        OAuthInfoDto oAuthInfoDto = oAuthInfoService.findByOAuthInfoId(id, sqlmapClient);
        EnvironmentDto environmentDto = environmentService.findByEnvironmentId(oAuthInfoDto.getEnvironmentId(), sqlmapClient);
        MerchantDto merchantDto = merchantService.findByMerchantId(oAuthInfoDto.getMerchantId(), sqlmapClient);

        if (oAuthInfoDto == null) {
            model.addAttribute("msg", "User Not Found");
        }

        model.addAttribute("mercId", merchantDto);
        model.addAttribute("envId", environmentDto);
        model.addAttribute("model", oAuthInfoDto);

        return "OAuthInfoShowSpecific";
    }

    /**
     * Delete Data by Id and it will redirect to /authinfos (dashboard) #
     */
    @RequestMapping(value = "/authinfos/delete/{id}", method = RequestMethod.GET)
    public String DeleteMerchantById(@PathVariable("id") String id) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        oAuthInfoService.deleteByOAuthInfoId(id, sqlmapClient);
        return "redirect:/authinfos";
    }

    /**
     * Show update Form #
     */
    @RequestMapping(value = "/authinfos/update/{id}", method = RequestMethod.GET)
    public String ShowFormUpdateMerchantById(@PathVariable String id, Model model) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        OAuthInfoDto oAuthInfoDto = oAuthInfoService.findByOAuthInfoId(id, sqlmapClient);
        List<EnvironmentDto> environmentDtoList = environmentService.getAllEnvironment(sqlmapClient);
        List<MerchantDto> merchantDtoList = merchantService.getAllMerchant(sqlmapClient);

        model.addAttribute("envList", environmentDtoList);
        model.addAttribute("mercList", merchantDtoList);
        model.addAttribute("authData", oAuthInfoDto);
        return "oAuthInfoForm";
    }

    /**
     * Update Data #
     */
    @RequestMapping(value = "/updateauthinfos", method = RequestMethod.POST)
    public String UpdateMerchantById(OAuthInfoDto oAuthInfoDto, BindingResult result, HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        if (result.hasErrors()) {
            return "redirect:/authinfos/update/" + oAuthInfoDto.getOauthClientId();
        } else {
            String oauthClientId = request.getParameter("oauthClientId");
            String oauthMerchantId = request.getParameter("oauthMerchantId");

            String oauthRedirectUrl = request.getParameter("oauthRedirectUrl");
            String oauthClientSecret = request.getParameter("oauthClientSecret");

            String merchantId = request.getParameter("merchantId");
            String environmentId = request.getParameter("environmentId");

            System.out.println(oauthClientId + " "+ oauthMerchantId + " " + oauthRedirectUrl + " "+ oauthClientSecret + " ");
            if (oauthClientId.isEmpty() || oauthMerchantId.isEmpty() || oauthRedirectUrl.isEmpty() || oauthClientSecret.isEmpty()) {
                return "redirect:/authinfos/update/" + oAuthInfoDto.getOauthClientId();
            }

            oAuthInfoDto.setEnvironmentId(environmentId);
            oAuthInfoDto.setMerchantId(merchantId);
            oAuthInfoDto.setOauthRedirectUrl(oauthRedirectUrl);
            oAuthInfoDto.setOauthClientId(oauthClientId);
            oAuthInfoDto.setOauthMerchantId(oauthMerchantId);
            oAuthInfoDto.setOauthClientSecret(oauthClientSecret);

            oAuthInfoService.updateByOAuthInfoId(oAuthInfoDto, sqlmapClient);
            return "redirect:/authinfos";
        }
    }

    /**
     * Show Create Form #
     */
    @RequestMapping(value = "/authinfos/create", method = RequestMethod.GET)
    public String ShowFormCreateMerchant(Model model) throws IOException{
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
        List<EnvironmentDto> environmentDtoList = environmentService.getAllEnvironment(sqlmapClient);
        List<MerchantDto> merchantDtoList = merchantService.getAllMerchant(sqlmapClient);

        model.addAttribute("envList", environmentDtoList);
        model.addAttribute("mercList", merchantDtoList);
        return "oAuthInfoForm";
    } // GANTI

    /**
     * push data #
     */
    @RequestMapping(value = "/createauthinfos", method = RequestMethod.POST)
    public String CreateMerchant(Model model, HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        OAuthInfoDto oAuthInfoDto = new OAuthInfoDto();

        String oauthClientId = request.getParameter("oauthClientId_");
        String oauthMerchantId = request.getParameter("oauthMerchantId_");

        String oauthRedirectUrl = request.getParameter("oauthRedirectUrl_");
        String oauthClientSecret = request.getParameter("oauthClientSecret_");

        String merchantId = request.getParameter("merchantId_");
        String environmentId = request.getParameter("environmentId_");

        if (oAuthInfoService.findByOAuthInfoId(oauthClientId, sqlmapClient) != null) {
            model.addAttribute("msg", "Data ID sudah ada sebelumnya");
            return "oAuthInfoForm";
        } else if (oauthClientId.isEmpty()|| oauthMerchantId.isEmpty() || oauthRedirectUrl.isEmpty() || oauthClientSecret.isEmpty()) {
            model.addAttribute("msg", "Form tidak boleh kosong");
            return "oAuthInfoForm";
        }

        oAuthInfoDto.setOauthClientId("OAC" + oauthClientId);
        oAuthInfoDto.setOauthMerchantId("MI" + oauthMerchantId);
        oAuthInfoDto.setOauthRedirectUrl(oauthRedirectUrl);
        oAuthInfoDto.setOauthClientSecret("OACS" + oauthClientSecret);
        oAuthInfoDto.setMerchantId(merchantId);
        oAuthInfoDto.setEnvironmentId(environmentId);

        oAuthInfoService.createOAuthInfo(oAuthInfoDto, sqlmapClient);
        return "redirect:/authinfos";
    }
}