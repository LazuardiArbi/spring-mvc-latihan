package com.mkyong.web.controller; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mkyong.web.dto.LoginDto;
import com.mkyong.web.service.Login.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Reader;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: LoginController.java, v 0.1 2019-03-15 15:31 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loginPageRedirect() {
        return "redirect:/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage(){
        return "loginPage";
    }

    @RequestMapping(value = "/loginSubmit", method = RequestMethod.POST)
    public String DoLogin(HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        LoginDto loginDto = new LoginDto();

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        loginDto.setUsername(username);
        loginDto.setPassword(password);

        if(loginService.findUsernameAndPassword(loginDto, sqlmapClient) != null){
            return "redirect:/dashboard";
        }
        else{
            return "loginPage";
        }
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String showDashboardPage(){
        return "dashboard";
    }
}